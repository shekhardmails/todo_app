import { createSlice } from '@reduxjs/toolkit';
import { todoInitialState } from './todoInitialState';

export const todoDetails = createSlice({
  name: 'todoDetails',
  initialState: todoInitialState,
  reducers: {
    addTodo: addTodo,
    deleteTodo: deleteTodo,
    replaceTodoList: replaceTodoList,
  },
});

function addTodo(state, action) {
  state.todoItems.push(action.payload.todoItem);
}

function deleteTodo(state, action) {
  let index = state.todoItems.findIndex(x => x === action.payload.item);
  if (index != -1) {
    state.todoItems.splice(index, 1);
  }
}

function replaceTodoList(state, action) {
  state.todoItems = action.payload.todoList
}
