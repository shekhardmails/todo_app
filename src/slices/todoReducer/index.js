import {todoDetails} from './todoSlice';

export const {addTodo, deleteTodo, replaceTodoList} = todoDetails.actions;

export default todoDetails.reducer;
