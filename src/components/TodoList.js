import React from 'react';
import {
  View,
  Text,
  Image,
  Alert,
  FlatList,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {useSelector, useDispatch} from 'react-redux';
import {deleteTodo, replaceTodoList} from '../slices/todoReducer';
import { DELETE_CONFIRM_MSG, LOCAL_STORAGE_TODO_LIST_KEY } from '../utils/StringConstants';

const TodoList = ({navigation}) => {
  const dispatch = useDispatch();

  const todoDetails = useSelector((state) => {
    return state.todoDetails;
  });

  onAddPress = () => {
    navigation.navigate('AddTodo');
  };

  onDeletePress = (item) => {
    Alert.alert(
      DELETE_CONFIRM_MSG,
      '',
      [
        {
          text: 'No',
          onPress: () => console.log('No Pressed'),
          style: 'cancel',
        },
        {
          text: 'Yes',
          onPress: () => {
            console.log('Yes Pressed');
            dispatch(deleteTodo({item: item}));
            deleteTodoFromLocal(item);
          },
        },
      ],
      {cancelable: false},
    );
  };

  deleteTodoFromLocal = (item) => {
    AsyncStorage.getItem(LOCAL_STORAGE_TODO_LIST_KEY).then((result) => {
      var todoData = JSON.parse(result);
      let index = todoData.findIndex((x) => x === item);
      if (index != -1) {
        todoData.splice(index, 1);
      }
      AsyncStorage.setItem(LOCAL_STORAGE_TODO_LIST_KEY, JSON.stringify(todoData));
    });
  };

  React.useEffect(() => {
    fetchDataFromLocal();
  }, []);

  fetchDataFromLocal = () => {
    AsyncStorage.getItem(LOCAL_STORAGE_TODO_LIST_KEY).then((result) => {
      var todoData = JSON.parse(result);
      if(todoData != null && todoData != undefined) {
        dispatch(replaceTodoList({todoList: todoData}));
      }
    });
  };

  return (
    <View style={styles.container}>
      <FlatList
        data={todoDetails.todoItems}
        showsHorizontalScrollIndicator={false}
        keyExtractor={(item, index) => `category-lists-${index}`}
        renderItem={({item, index}) => (
          <View style={styles.itemStyle} key={index}>
            <View style={styles.itemContainer}>
              <Text style={styles.todoTitleTextStyle}>{item}</Text>
              <TouchableOpacity onPress={() => onDeletePress(item)}>
                <Image
                  style={{height: 25, width: 25}}
                  source={require('./../../assets/delete.png')}
                />
              </TouchableOpacity>
            </View>
          </View>
        )}
      />
      <TouchableOpacity
        style={styles.floatingBtnStyle}
        onPress={() => onAddPress()}>
        <Text style={styles.addTextStyle}>+</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  floatingBtnStyle: {
    width: 50,
    height: 50,
    backgroundColor: '#02b970',
    position: 'absolute',
    bottom: 30,
    right: 30,
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },
  addTextStyle: {
    fontSize: 32,
    fontWeight: '400',
    color: 'white',
  },
  itemStyle: {
    height: 90,
    backgroundColor: 'white',
    justifyContent: 'center',
  },
  itemContainer: {
    elevation: 3,
    borderRadius: 5,
    padding: 10,
    shadowColor: '#444',
    shadowOpacity: 0.5,
    shadowRadius: 8,
    backgroundColor: '#02b970',
    marginHorizontal: '5%',
    height: '60%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  todoTitleTextStyle: {
    fontSize: 18,
    color: 'white',
    fontWeight: '500',
  },
});

export default TodoList;
