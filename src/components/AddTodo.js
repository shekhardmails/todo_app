import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  Alert,
  TextInput,
} from 'react-native';
import {addTodo} from '../slices/todoReducer';
import AsyncStorage from '@react-native-community/async-storage';
import {useDispatch} from 'react-redux';
import {
  EMPTY_TODO_ITEM_MSG,
  ITEM_ADDED_SUCCESS_MSG,
  LOCAL_STORAGE_TODO_LIST_KEY,
} from '../utils/StringConstants';

const AddTodo = (navigation) => {
  const [todoText, setTodoText] = useState('');
  const dispatch = useDispatch();

  onSubmitPress = () => {
    if(todoText == '') {
      Alert.alert(EMPTY_TODO_ITEM_MSG)
    }
    else {
      dispatch(addTodo({todoItem: todoText}));
      Alert.alert(ITEM_ADDED_SUCCESS_MSG);
      updateLocalList();  
    }
  };

  updateLocalList = () => {
    AsyncStorage.getItem(LOCAL_STORAGE_TODO_LIST_KEY).then((result) => {
      var todoData = JSON.parse(result);
      if (todoData == null || todoData == undefined) {
        todoData = [];
      }
      todoData.push(todoText);
      AsyncStorage.setItem(
        LOCAL_STORAGE_TODO_LIST_KEY,
        JSON.stringify(todoData),
      );
      setTodoText('');
    });
  };

  return (
    <View style={styles.container}>
      <View style={styles.subcontainerStyle}>
        <TextInput
          value={todoText}
          onChangeText={(text) => setTodoText(text)}
          placeholder={'To do'}
          style={styles.txtInputStyle}></TextInput>
        <TouchableOpacity
          style={styles.submitBtnStyle}
          onPress={() => onSubmitPress()}>
          <Text style={styles.submitBtnTxtStyle}>Submit</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#02b970',
    flexDirection: 'row',
  },
  subcontainerStyle: {
    elevation: 3,
    height: 230,
    borderRadius: 15,
    padding: 10,
    shadowColor: '#444',
    shadowOffset: {width: 0, height: 10},
    shadowOpacity: 0.5,
    shadowRadius: 8,
    backgroundColor: 'lightgray',
    marginHorizontal: '10%',
    flex: 1,
    marginTop: '10%',
    justifyContent: 'space-around',
  },
  txtInputStyle: {
    height: '30%',
    backgroundColor: 'white',
    marginHorizontal: '10%',
    padding: 10,
    fontSize: 15,
    fontWeight: '500',
    borderRadius: 10,
  },
  submitBtnStyle: {
    height: 50,
    width: 150,
    backgroundColor: 'white',
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    borderColor: '#02b970',
    borderWidth: 2
  },
  submitBtnTxtStyle: {
    fontSize: 20,
    fontWeight: '500',
    color: '#02b970'
  },
});

export default AddTodo;
