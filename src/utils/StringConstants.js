//MSGS
export const DELETE_CONFIRM_MSG = 'Do you want to delete Todo item?';
export const EMPTY_TODO_ITEM_MSG = 'Please enter value first';
export const ITEM_ADDED_SUCCESS_MSG = 'To Do item added successfully.';


//KEYS
export const LOCAL_STORAGE_TODO_LIST_KEY = 'todoList_data';
