import {combineReducers} from '@reduxjs/toolkit';
import todoDetails from '../slices/todoReducer';

const rootReducer = combineReducers({
  todoDetails,
});

export default rootReducer;
